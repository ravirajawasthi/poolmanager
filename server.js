/* ----------------------------- Imports -----------------------------*/
const express = require("express");
var bodyParser = require("body-parser");
const fileUpload = require("express-fileupload");
const os = require("os");
const path = require("path");
const {
  newMember,
  getAllMembers,
  updateMemberDetails,
  changeMemberImage,
} = require("./handlers/member");

/* ----------------------------- App config -----------------------------*/
const app = express();
const port = 5000;
app.use(bodyParser.json()); // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // to support URL-encoded bodies
app.use("/static", express.static(path.join(__dirname, "uploads")));

/* ----------------------------- Express-upload config -----------------------------*/
app.use(
  fileUpload({
    limits: { fileSize: 50 * 1024 * 1024 },
    useTempFiles: true,
    tempFileDir: os.tmpdir(),
  })
);

/* ----------------------------- Routes -----------------------------*/

app.post("/members", newMember);
app.get("/members", getAllMembers);
app.post("/members/:id", updateMemberDetails);
app.post("/member/image/:id", changeMemberImage);

/* ----------------------------- Server config -----------------------------*/
app.listen(port, () => console.log(`Server started at localhost:${port}`));
