let { mongoose, AutoIncrement } = require("../util/databaseConfig");
var uniqueValidator = require("mongoose-unique-validator");

let memberSchema = new mongoose.Schema({
  id: { type: Number, unique: true },
  expDate: { type: Date, required: true },
  formNo: { type: Number, unique: true, require: true },
  entryDate: { type: Date, unique: true, default: Date.now },
  reciptNo: { type: Number, unique: true, require: true },
  gender: String,
  name: { type: String, required: true },
  guardian: String,
  dob: { type: Date, require: true },
  address: String,
  mob: Number,
  pincode: { type: Number, required: true },
  imageUrl: String,
  fees: Number,
  membershipType: { type: String, required: true },
  timing: { type: String, required: true },
  bloodGroup: String,
  occupation: String
});

memberSchema.plugin(uniqueValidator);
memberSchema.plugin(AutoIncrement, { inc_field: "id" });
const Member = new mongoose.model("Member", memberSchema);

module.exports = Member;
