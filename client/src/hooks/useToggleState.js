import {useState} from 'react'

function useToggleState(initialValue = false){
    const [value, setValue] = useState(initialState)
    const toggle = () => setValue(!value)
}

export default useToggleState