import { useState } from "react";

function useInputState(defaultValue = "") {
  const [value, setValue] = useState(defaultValue);
  const reset = () => {
      setValue("");
    },
    handleChange = e => {
      setValue(e.target.value);
    };
  return {value, handleChange, reset};
}
export default useInputState;