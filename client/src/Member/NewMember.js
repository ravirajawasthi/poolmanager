import React from "react";
import NewMemberForm from "./NewMemberForm";
import axios from "axios";
import placeHolderImage from "./images/placeholder.png";
import styles from "./styles/NewMemberStyles";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";
import { useState } from "react";
import { withStyles } from "@material-ui/core/styles";

function NewMember(props) {
  console.log("rendering");
  const { classes } = props;
  const [image, setImage] = useState(placeHolderImage);
  const [errors, setErrors] = useState({});

  const [flash, setFlash] = useState({
    variant: "",
    message: undefined
  });

  const handleFormSubmit = formData => {
    const form = new FormData();
    Object.keys(formData).map(key => form.append(key, formData[key]));
    const imageInput = document.getElementById("image");
    const memberImage = imageInput.files[0];
    if (memberImage && memberImage.type.split("/")[0] === "image") {
      form.append("image", memberImage);
      axios
        .post("/members", form)
        .then(function() {
          console.log("member created");
          setFlash({
            variant: "success",
            message: "New Member Created",
            open: true
          });
          setErrors({})
        })
        .catch(function(error) {
          setFlash({
            variant: "error",
            message: "Invalid Information Provided",
            open: true
          });
          setErrors({ ...error.response.data.formError });
          console.log( {...error.response.data.formError });
          console.log("Member information not unique");
        });
    } else {
      setFlash({
        variant: "error",
        message: "Invalid image selected",
        open: true
      });
    }
  };

  const onImageChange = e => {
    const file = e.target.files[0];
    if (file && file.type.split("/")[0] === "image") {
      let fileReader = new FileReader();
      fileReader.readAsDataURL(file);
      fileReader.onload = e => {
        setImage(fileReader.result);
      };
    } else {
      setFlash({
        variant: "error",
        message: "Invalid image selected",
        open: true
      });
    }
  };
  return (<>
    <div className={classes.mainContainer}>
      <div className={classes.photoUploadContainer}>
        <Snackbar
          open={flash.open}
          autoHideDuration={6000}
          onClose={() => setFlash({ open: false })}
        >
          <Alert variant="filled" severity={flash.variant}>
            {flash.message}
          </Alert>
        </Snackbar>
        <img src={image} className={classes.uploadedImage} alt="placeholder" />
        <input type="file" id="image" onChange={onImageChange} required />
      </div>
      <NewMemberForm errors={errors} handleFormSubmit={handleFormSubmit} />
    </div></>
  );
}

export default withStyles(styles)(NewMember);
