import React, { useState } from "react";
import styles from "./styles/MemberInfoStyles";
import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import EditMember from "./EditMember";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import { getMembershipType, getDateString } from "../helper";
import Axios from "axios";

function MemberInfo({
  _id,
  name,
  membershipType,
  gender,
  expDate,
  classes,
  imageUrl,
  address,
  id,
  UpdateMemberDetails,
  updateMemberState
}) {
  let imageSrc = imageUrl => `http://localhost:5000/static/${imageUrl}`;
  const [image, setImage] = useState(imageSrc(imageUrl));
  const [open, setOpen] = useState(false);
  const openDialog = () => setOpen(true);
  const closeDialog = () => setOpen(false);

  const openImageDialog = () => {
    const imageInput = document.getElementById("imageInput");
    imageInput.click();
  };

  const handleImageChange = e => {
    const newImage = e.target.files[0];
    if (newImage && newImage.type.split("/")[0] === "image") {
      let fileReader = new FileReader();
      fileReader.readAsDataURL(newImage);
      fileReader.onload = e => {
        setImage(fileReader.result);
      };
      const formObj = new FormData();
      console.log("current Image url", imageUrl)
      formObj.append("image", newImage);
      Axios.post(`/member/image/${_id}`, formObj).then(res => {
        if (res.data.verdict) {
          console.log("current Image url", res.data.result.imageUrl)
          UpdateMemberDetails(_id, res.data.result);
        }
      });
    }
  };
  return (
    <div className={classes.root}>
      <EditMember
        name={name}
        UpdateMemberDetails={UpdateMemberDetails}
        id={id}
        _id={_id}
        address={address}
        closeDialog={closeDialog}
        open={open}
      />
      <div className={classes.imageContainer}>
        <img src={image} alt="member" className={classes.image} />
        <IconButton
          color="secondary"
          className={classes.changeImage}
          onClick={openImageDialog}
        >
          <EditIcon />
        </IconButton>
      </div>
      <form>
        <input
          type="file"
          hidden={true}
          id="imageInput"
          onChange={handleImageChange}
        />
      </form>
      <Typography variant="body1">Member ID : {id}</Typography>
      <Typography variant="body1">Name : {name}</Typography>
      <Typography variant="body1">
        Membership : {getMembershipType(membershipType)}
      </Typography>
      <Typography variant="body1">Gender : {gender}</Typography>
      <Typography variant="body1">
        Expiry Date : {getDateString(expDate)}
      </Typography>
      <Typography variant="body1">Address : {address}</Typography>
      <Button onClick={openDialog} variant="contained" color="secondary">
        Edit
      </Button>
    </div>
  );
}

export default withStyles(styles)(MemberInfo);
