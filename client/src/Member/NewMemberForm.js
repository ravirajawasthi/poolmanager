import React from "react";

import styles from "./styles/NewMemberFormStyles";

import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Divider from "@material-ui/core/Divider";
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import useInputState from "../hooks/useInputState";
import { withStyles } from "@material-ui/core/styles";

function NewMemberForm({ classes, handleFormSubmit, errors }) {
  const currentDate = new Date();
  const dateString = currentDate => {
    const d = new Date(currentDate);
    return `${d.getDate()}/${d.getMonth()}/${d.getFullYear()}`;
  };
  const inputFields = {
    membershipType: useInputState("GB"),
    date: useInputState(dateString),
    formNo: useInputState(""),
    reciptNo: useInputState(""),
    gender: useInputState("female"),
    name: useInputState(""),
    guardian: useInputState(""),
    dob: useInputState(""),
    mob: useInputState(""),
    pincode: useInputState(""),
    fees: useInputState(0),
    address: useInputState(""),
    timing: useInputState("6M"),
    bloodGroup: useInputState(""),
    occupation: useInputState("")
  };

  const getCategoryInfo = membershipType => {
    switch (membershipType) {
      case "GB":
      case "GS":
      case "SB":
      case "SS":
      case "F":
      default:
        return new Date(
          `31 November ${currentDate.getFullYear()}`
        ).toISOString();
      case "M":
        return new Date(
          currentDate.setMonth(currentDate.getMonth() + 2)
        ).toISOString();
    }
  };

  const handleSubmit = e => {
    e.preventDefault();
    let formData = {};
    Object.keys(inputFields).map(
      key => (formData[key] = inputFields[key].value)
    );
    formData.expDate = dateString(
      getCategoryInfo(inputFields.membershipType.value)
    );
    handleFormSubmit(formData);
  };
  return (
    <Paper className={classes.formContainer}>
      <Typography variant="h4" className={classes.formTitle}>
        Personal Information
      </Typography>
      <Divider className={classes.divider} />
      <form onSubmit={handleSubmit}>
        <FormControl className={classes.inputField}>
          <InputLabel>Member Type</InputLabel>
          <Select
            name="membershipType"
            value={inputFields["membershipType"].value}
            onChange={inputFields["membershipType"].handleChange}
          >
            <MenuItem value="GB"> General Beginner </MenuItem>
            <MenuItem value="GS"> General Swimmer </MenuItem>
            <MenuItem value="SB"> Student Beginner </MenuItem>
            <MenuItem value="SS"> Student Swimmer </MenuItem>
            <MenuItem value="F"> Family</MenuItem>
            <MenuItem value="M"> Monthly</MenuItem>
          </Select>
        </FormControl>
        
          <TextField
          className={classes.dateField}
            required
            type="date"
            name="dob"
            value={inputFields["dob"].value}
            onChange={inputFields["dob"].handleChange}
          />
        
        <FormControl className={classes.inputField}>
          <InputLabel>Gender</InputLabel>
          <Select
            name="gender"
            required
            value={inputFields["gender"].value}
            onChange={inputFields["gender"].handleChange}
          >
            <MenuItem value="female">Female</MenuItem>
            <MenuItem value="male">Male</MenuItem>
          </Select>
        </FormControl>
        <TextField
          className={classes.dateField}
          value={dateString(getCategoryInfo(inputFields.membershipType.value))}
          disabled
          size="small"
          helperText="Expiry Date"
        />
        <TextField
          required
          label="Form No"
          placeholder="Eg: 111"
          className={classes.inputField}
          error={Boolean(errors.formNo)}
          helperText="Must be Unique"
          type="number"
          name="formNo"
          value={inputFields["formNo"].value}
          onChange={inputFields["formNo"].handleChange}
        />
        <TextField
          required
          label="Recipt No"
          placeholder="Eg: 111"
          className={classes.inputField}
          type="number"
          name="reciptNo"
          error={Boolean(errors.reciptNo)}
          helperText="Must be Unique"
          value={inputFields["reciptNo"].value}
          onChange={inputFields["reciptNo"].handleChange}
        />

        <TextField
          required
          label="Name"
          placeholder="Eg: Rajesh Kumar"
          className={classes.inputField}
          type="text"
          name="name"
          value={inputFields["name"].value}
          onChange={inputFields["name"].handleChange}
        />
        <TextField
          type="text"
          label="Guadian"
          placeholder="Parent / Guardian"
          name="guardian"
          className={classes.inputField}
          value={inputFields["guardian"].value}
          onChange={inputFields["guardian"].handleChange}
        />

        <TextField
          required
          label="Mobile No."
          placeholder="Eg: 9988774455"
          min="5000000000"
          max="9999999999"
          type="tel"
          name="mob"
          className={classes.inputField}
          value={inputFields["mob"].value}
          onChange={inputFields["mob"].handleChange}
        />
        <TextField
          required
          label="Pincode"
          min="100000"
          max="999999"
          placeholder="Eg: 462011"
          type="number"
          name="pincode"
          className={classes.inputField}
          value={inputFields["pincode"].value}
          onChange={inputFields["pincode"].handleChange}
        />
        <TextField
          required
          label="Fees"
          placeholder="Eg: 5000"
          type="number"
          name="fees"
          className={classes.inputField}
          value={inputFields["fees"].value}
          onChange={inputFields["fees"].handleChange}
        />
        <FormControl className={classes.inputField}>
          <InputLabel>Timing</InputLabel>
          <Select
            name="timing"
            required
            value={inputFields["timing"].value}
            onChange={inputFields["timing"].handleChange}
          >
            <MenuItem value="" disabled>
              Moring
            </MenuItem>
            <MenuItem value="6M">6:00 AM - 6:45 AM</MenuItem>
            <MenuItem value="7M">7:00 AM - 7:45 AM</MenuItem>
            <MenuItem value="8M">8:00 AM - 8:45 AM</MenuItem>
            <MenuItem value="9M">9:00 AM - 9:45 AM</MenuItem>
            <MenuItem value="10M">10:00 AM - 10:45 AM</MenuItem>
            <MenuItem value="" disabled>
              Moring
            </MenuItem>
            <MenuItem value="4E">4:00 PM - 4:45 PM</MenuItem>
            <MenuItem value="5E">5:00 PM - 5:45 PM</MenuItem>
            <MenuItem value="6E">6:00 PM - 6:45 PM</MenuItem>
            <MenuItem value="7E">7:00 PM - 7:45 PM</MenuItem>
            <MenuItem value="8E">8:00 PM - 8:45 PM</MenuItem>
            <MenuItem value="9E">9:00 PM - 9:45 PM</MenuItem>
          </Select>
        </FormControl>
        <TextField
          type="text"
          label="Blood Group"
          placeholder="B+"
          name="bloodGroup"
          className={classes.inputField}
          value={inputFields["bloodGroup"].value}
          onChange={inputFields["bloodGroup"].handleChange}
        />
        <TextField
          type="text"
          label="Occupation"
          name="bloodGroup"
          className={classes.inputField}
          value={inputFields["occupation"].value}
          onChange={inputFields["occupation"].handleChange}
        />
        <TextField
          required
          type="text"
          label="Address"
          placeholder="Eg: E1/1 Arera Colony"
          name="address"
          multiline
          rows={3}
          className={`${classes.address} ${classes.inputField}`}
          value={inputFields["address"].value}
          onChange={inputFields["address"].handleChange}
        />

        <Button
          color="primary"
          variant="contained"
          type="submit"
          className={classes.address}
        >
          Submit
        </Button>
      </form>
    </Paper>
  );
}

export default withStyles(styles)(NewMemberForm);
