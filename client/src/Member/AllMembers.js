import React, { useState } from "react";
import styles from "./styles/AllMembersStyles";

import MemberInfo from "./MemberInfo";

import Axios from "axios";

import withStyles from "@material-ui/core/styles/withStyles";
import TextField from "@material-ui/core/TextField";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import Paper from "@material-ui/core/Paper";

import Fuse from "fuse.js";
import { Typography, Divider } from "@material-ui/core";

const fuseOptions = {
  keys: ["id", "name"],
  maxPatternLength: 25
};

function AllMembers({ classes }) {
  const [state, setState] = useState({
    membersArray: [],
    loading: true,
    error: false,
    search: "",
    firstLoad: true,
    member: {}
  });
  //custom component did mount
  if (state.firstLoad) {
    console.log("GG");
    Axios.get("/members")
      .then(res =>
        setState({
          ...state,
          membersArray: res.data.result,
          loading: false,
          firstLoad: false
        })
      )
      .catch(err => {
        console.log(err.response);
        setState({ ...state, loading: false, error: true, firstLoad: false });
      });
  }

  const fuse = new Fuse(state.membersArray, fuseOptions);
  const searchResult =
    state.search.length > 0 ? fuse.search(state.search) : state.membersArray;

  const UpdateMemberState = (id, memberObj) => {
    let currentMembers = state.membersArray;
    currentMembers = currentMembers.map(member => {
      if (member._id === id) {
        return memberObj;
      } else {
        return member;
      }
    });
    setState({
      ...state,
      membersArray: currentMembers,
      member: memberObj
    });
  };

  const UpdateMemberDetails = (id, fields) => {
    Axios.post(`/members/${id}`, { ...fields })
      .then(res => {
        if (res.data.verdict) {
          UpdateMemberState(id, res.data.result);
        }
      })
      .catch(err => console.log(err.response.data));
  };
  const handleClick = _id => {
    searchResult.map(member => {
      if (member._id === _id) {
        setState({ ...state, member: member });
        return member;
      }
      return member;
    });
  };
  return (
    <div className={classes.container}>
      {state.loading ? (
        <h1>Loading</h1>
      ) : state.error ? (
        <h1>Computers are dumb</h1>
      ) : (
        <>
          <Paper className={classes.listContainer}>
            {searchResult.length === 0 ? (
              <h1>No Members</h1>
            ) : (
              <>
                <TextField
                  label="Search Members"
                  error={state.search.length > 25}
                  helperText={"Use Name or Member ID"}
                  value={state.search}
                  name="search"
                  onChange={e => setState({ ...state, search: e.target.value })}
                />
                <List>
                  {searchResult.map(member => (
                    <ListItem button onClick={() => handleClick(member._id)}>
                      <ListItemAvatar>
                        <Avatar src={`/static/${member.imageUrl}`} />
                      </ListItemAvatar>
                      <ListItemText
                        primary={member.name}
                        secondary={`ID : ${member.id}`}
                      />
                    </ListItem>
                  ))}
                </List>
              </>
            )}
          </Paper>
          <Paper className={classes.memberDetails}>
            <Typography variant="h3" className={classes.header}>
              Member Details
            </Typography>
            <Divider className={classes.divider} />
            {Object.keys(state.member).length === 0 ? (
              <Typography color="textSecondary">No Member selected</Typography>
            ) : (
              <h1>
                <MemberInfo
                  {...state.member}
                  UpdateMemberDetails={UpdateMemberDetails}
                  UpdateMemberState={UpdateMemberState}
                />
              </h1>
            )}
          </Paper>
        </>
      )}
    </div>
  );
}

export default withStyles(styles)(AllMembers);
