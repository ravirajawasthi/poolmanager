const styles = theme => ({
    root: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        padding: "10px 0px"
    },
    image: {
        width: "100%",
        margin: "auto",
        maxHeight: "300px",
        objectFit: "contain",
        padding: "5px"
    },
    imageContainer :{
        position: "relative"
    },
    changeImage: {
        position: "absolute",
        bottom: 0,
        right: "0px",
        background: "white",
    }
})

export default styles