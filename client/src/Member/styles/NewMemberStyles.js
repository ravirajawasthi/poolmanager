

const styles = theme => ({

  mainContainer: {
    marginTop: "64px",
    width: "75%",
    height: "100vh",
    marginTop: "-64px",
    boxSizing: "border-box",
    display: "flex",
    margin: "auto",
    justifyContent: "space-around",
    alignItems: "center",
    [theme.breakpoints.down("sm")]: {
      flexWrap: "wrap"
    },
  },
  photoUploadContainer: {
    height: "80%",
    width: "25%",
    minWidth: "300px",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    padding: `${theme.spacing(1)}px`,
    '& input[type="file"]': {
      width: "60%",
      marginBottom: theme.spacing(2)
    }
  },
  uploadedImage: {
    width: "90%",
    borderRadius: "20%",
    marginBottom: theme.spacing(2)
  },
  Navbar: {
    height: "64px",
  }
});

export default styles;
