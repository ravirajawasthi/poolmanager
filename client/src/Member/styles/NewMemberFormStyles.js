const styles = theme => ({
    formContainer: {
        width: "75%",
        padding: theme.spacing(3),
        [theme.breakpoints.down("md")]: {
          height: "70%"
        }
      },
      formTitle: {
        textAlign: "center",
        padding: theme.spacing(2),
        marginBottom: theme.spacing(1)
      },
      divider: {
        marginBottom: `${theme.spacing(4)}px`
      },
      
      inputField: {
        margin: `${theme.spacing(3)}px ${theme.spacing(3)}px`
      },
      address: {
        margin: `${theme.spacing(1)}px ${theme.spacing(3)}px`,
        width: "95%"
      },
      dateField: {
        margin: `${theme.spacing(5)}px ${theme.spacing(3)}px`,
      }
})

export default styles