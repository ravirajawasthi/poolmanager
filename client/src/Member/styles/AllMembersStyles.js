const styles = (theme) => ({
    container: {
        width: "80%",
        height: "100%",
        margin: "auto",
        display: "flex",
        justifyContent: "space-around",
        padding: `${theme.spacing(4)}px 0`
    },
    listContainer: {
        width: "30%",
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
        alignSelf: "center",
        padding: `${theme.spacing(3)}px ${theme.spacing(1)}px`
    },
    memberDetails: {
        width: "50%"    
    },
    divider: {
        margin: `${theme.spacing(3)}px 0`
    },
    header: {
        margin: `${theme.spacing(3)}px 0`
    }
})

export default styles