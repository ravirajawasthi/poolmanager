import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";

export default function EditMember({
  open,
  closeDialog,
  name,
  id,
  address,
  UpdateMemberDetails,
  _id
}) {
  const [state, setState] = useState({ name, id, address });

  const handleChange = e =>
    setState({ ...state, [e.target.name]: e.target.value });

  const handleSubmit = (e) => {
    e.preventDefault()
    UpdateMemberDetails(_id, { ...state });
  };

  return (
    <div>
      <Dialog open={open} onClose={closeDialog}>
        <DialogTitle>Edit Details</DialogTitle>
        <DialogContent>
          <form onSubmit={handleSubmit}>
            <TextField
            margin="normal"
              autoFocus
              required
              onChange={handleChange}
              value={state.id}
              name="id"
              margin="dense"
              id="name"
              label="ID"
              type="number"
              fullWidth
            />
            <TextField
            margin="normal"
              onChange={handleChange}
              value={state.name}
              name="name"
              label="Name"
              type="text"
              required
              fullWidth
            />
            <TextField
            margin="normal"
              onChange={handleChange}
              value={state.address}
              name="address"
              label="address"
              multiline
              type="text"
              fullWidth
              required
            />
            <Button type="submit" onClick={closeDialog} color="primary" variant="contained">
              Update!
            </Button>
          </form>
        </DialogContent>
        <DialogActions></DialogActions>
      </Dialog>
    </div>
  );
}
