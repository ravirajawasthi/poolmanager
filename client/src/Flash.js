import React from "react";
import FlashMessage from "react-flash-message";
import { withStyles } from "@material-ui/core/styles";

const styles = props => ({
  message: {
    color: props.variant === "success" ? "darkgreen" : "white"
  },
  container: {
    background: props.variant === "success" ? "green" : "red",
    textAlign: "center"
  }
});

function Flash({ message, classes, duration }) {
  return (
    <FlashMessage
      duration={duration}
      persistOnHover={true}
    >
      <p className={`${classes.message} ${classes.container}`}>{message}</p>
    </FlashMessage>
  );
}

export default withStyles(styles)(Flash);
