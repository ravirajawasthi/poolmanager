import React from "react";
import Landing from "./Landing";
import NewMember from "./Member/NewMember";
import { Switch, Route } from "react-router-dom";
import Navbar from "./Navbar";
import "./App.css";
import AllMembers from "./Member/AllMembers";


//Photo by Gentrit Sylejmani on Unsplash

function App() {
  return (
    <div className="App">
      <Navbar />
      <div className="App-page">
      <Switch>
        <Route path="/" exact render={() => <Landing />} />
        <Route path="/members/new" exact render={() => <NewMember />} />
        <Route path="/members" exact render={() => <AllMembers />} />
      </Switch>
      </div>
    </div>
  );
}

export default App;
