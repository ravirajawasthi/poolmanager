import React from "react";

const Landing = (props) => {
  return (
    <>
      <h1>Major Project (Group 2)</h1>
      <h3>Raviraj Awasthi (0105CS171083)</h3>
      <h3>Nikhil Bajaj (0105CS171066)</h3>
      <h3>Siddhant Jain (0105CS171106)</h3>
      <h3>Pranav Nagar (0105CS171075)</h3>
    </>
  );
};

export default Landing;
