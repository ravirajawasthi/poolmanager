exports.getMembershipType = type => {
  switch (type) {
    case "GB":
      return "General Beginner";
    case "GS":
      return "General Swimmer";
    case "SB":
      return "Student Beginner";
    case "SS":
      return "Studnet Swimmer";
    case "M":
      return "Monthly";
    case "F":
      return "Family";
    default:
      return undefined;
  }
};

exports.getDateString = (dateStr) => {
  const d = new Date(dateStr)
  return `${d.getDate()}/${d.getMonth() + 1}/${d.getFullYear()}`
}