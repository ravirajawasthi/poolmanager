// Check file type of uploaded image
exports.checkFileType = (mimeType) => {
  if (mimeType.split("/")[0] === "image") {
    return mimeType.split("/")[1];
  } else {
    return false;
  }
};
