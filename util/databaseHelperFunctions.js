const {mongoose} = require("./databaseConfig")

exports.getMemberId = async () => {
    let id;
    await Global.findOne({}, (err, res) => {
        if (err) return false
        else id = res["MEMBERNUMBER"]
    })
    return id
}