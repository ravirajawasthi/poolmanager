/* ----------------------------- Database -----------------------------*/
var mongoose = require("mongoose");
const AutoIncrementFactory = require("mongoose-sequence");
require("dotenv").config();

/* ----------------------------- Mongoose Connectivity -----------------------------*/
mongoose.connect(process.env.MONGO_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
});

var db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", function () {
  console.log("we're up and running with mongodb server");
});

const AutoIncrement = AutoIncrementFactory(db);
module.exports = { db, mongoose, AutoIncrement };
