const Member = require("../models/memberModel");

const path = require("path");

const { checkFileType, validateNewMemberData } = require("../util/helper");

exports.newMember = async (req, res) => {
  let responseObj = {};
  if (Object.keys(req.files).length === 0) {
    responseObj.verdict = false;
    responseObj.error = "No Image was sent!";
    return res.status(401).json(responseObj);
  } else {
    const memberImage = req.files.image;
    const ext = checkFileType(memberImage.mimetype);
    let filename;
    if (ext) {
      filename = `${Math.round(Math.random() * 100000000) + 10000}.${ext}`;
    } else {
      responseObj.verdict = false;
      responseObj.error = "Invalid file Choosen!";
      return res.status(402).json(responseObj);
    }
    const savePath = path.join(__dirname, "..", "uploads", filename);
    memberImage.mv(savePath, function (err) {
      if (err) {
        console.log(err);
        responseObj.verdict = false;
        responseObj.error =
          "Servers not behaving as expected! Please check Internet connectivity and try again after sometime";
        return res.status(500).json(responseObj);
      } else {
        const newMember = req.body;
        newMember.imageUrl = filename;
        Member.create(newMember, (err, createdMember) => {
          if (err) {
            (responseObj.verdict = false), (responseObj.formError = err.errors);
            return res.status(402).json(responseObj);
          } else {
            (responseObj.verdict = true), (responseObj.result = createdMember);
            return res.json(responseObj);
          }
        });
      }
    });
  }
};

exports.getAllMembers = async (req, res) => {
  let responseObj = {};
  await Member.find({}, (err, allMembersArray) => {
    if (err) {
      responseObj.verdict = false;
      responseObj.error = err;
      return res.status(500).json(responseObj);
    } else {
      responseObj.result = allMembersArray;
      responseObj.verdict = true;
      return res.json(responseObj);
    }
  });
};

exports.updateMemberDetails = async (req, res) => {
  const responseObj = {};
  const memberId = req.params.id;
  const fieldsToBeUpdated = req.body;
  Member.findByIdAndUpdate(
    memberId,
    { $set: { ...fieldsToBeUpdated } },
    (err, updatedMember) => {
      if (err) {
        responseObj.verdict = false;
        responseObj.error = "Something went wrong, please try again!";
        console.log(err);
        res.status(500).json(responseObj);
      } else {
        responseObj.verdict = true;
        responseObj.result = { ...updatedMember._doc, ...fieldsToBeUpdated };
        res.json(responseObj);
      }
    }
  );
};

exports.changeMemberImage = async (req, res) => {
  let responseObj = {};
  const id = req.params.id;
  const memberImage = req.files.image;
  const ext = checkFileType(memberImage.mimetype);
  let filename;
  if (ext) {
    filename = `${id}.${ext}`;
  } else {
    responseObj.verdict = false;
    responseObj.error = "Invalid file Choosen!";
    return res.status(402).json(responseObj);
  }
  const savePath = path.join(__dirname, "..", "uploads", filename);
  memberImage.mv(savePath, (err) => {
    if (err) {
      responseObj.verdict = false;
      responseObj.error = "File Upload Failed, Server Error. Please Try again";
      res.json(responseObj);
    } else {
      Member.findByIdAndUpdate(
        id,
        { $set: { imageUrl: filename } },
        (err, updatedMember) => {
          if (err) {
            responseObj.verdict = false;
            responseObj.error =
              "Image Uploaded, but failed to update member image field";
          }
          responseObj.verdict = true;
          responseObj.result = { ...updatedMember._doc, imageUrl: filename };
          res.json(responseObj);
        }
      );
    }
  });
};
